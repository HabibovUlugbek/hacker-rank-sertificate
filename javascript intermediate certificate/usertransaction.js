const axios = require("axios")
async function getNumTransactions(username) {
    try{
        const {data} = await axios.get(`https://jsonmock.hackerrank.com/api/article_users?username=${username}`)
        if(data.data && data.data.length !==0) {
            const userId = data.data[0].id; 
             const res =await axios.get(`https://jsonmock.hackerrank.com/api/transactions?&userId=${userId}`)
            return res.data.total;
        }else{
            return "Username Not Found"
        } 
    } catch(err){
        console.log(err)
    }
    // API endpoint: https://jsonmock.hackerrank.com/api/article_users?username=<username>
    // API endpoint: https://jsonmock.hackerrank.com/api/transactions?&userId=<userId>
}