function joinedLogger(level, sep) {
    let result = "";
    let addSep = true
  return (...messages) => {
      for(let i=0; i<messages.length; i++){
          if(messages[i].level>= level) {
              if(addSep){
                  addSep= false
              }else {
                  result+=sep;
              }
              result += messages[i].text
          }
      }
      logger({text:result})
  }
}