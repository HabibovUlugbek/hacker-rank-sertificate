function diagonalDifference(arr) {
    let left = 0 ;
    let right = 0;
    for(let i =0 ;i<arr.length; i++){
      for(let j =0 ;j<arr[i].length; j++){
          if(i===j) left = left + arr[i][j];
          if(arr.length-1 === i+j) right = right +arr[i][j];
        }  
    }
    return Math.abs(right -left)
}
